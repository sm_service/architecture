﻿using UnityEngine;

namespace MCOV
{
    public class AView : MonoBehaviour
    {
        private void Awake()
        {
            Initialize();
        }

        protected virtual void Initialize()
        {
            
        }
        
        protected virtual void OnEnable()
        {
            Subscribe();
        }
        
        protected virtual void OnDisable()
        {
            Unsubscribe();
        }

        protected virtual void Subscribe()
        {
            
        }
        
        protected virtual void Unsubscribe()
        {
            
        }
    }
}