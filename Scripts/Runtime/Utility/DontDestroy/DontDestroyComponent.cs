using UnityEngine;

namespace Utility.DontDestroy
{
    public class DontDestroyComponent : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}
