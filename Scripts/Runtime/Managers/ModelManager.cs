﻿using System;
using System.Collections.Generic;
using System.Linq;
using MCOV;
using UnityEngine;

namespace Managers
{
    [Serializable]
    public class DebugData
    {
        public string Name;
        [SerializeReference]
        public AData Data;

        public DebugData(AData data)
        {
            Name = data.GetType().Name;
            Data = data;
        }
    }

    public class ModelManager : MonoBehaviour
    {
        
        private List<AModel> _listModels = new List<AModel>();
        
        private Dictionary<string, AData> _loadedData = new Dictionary<string,AData>();

        [SerializeReference]
        public List<object> _debugListData = new List<object>();
        

        private void Awake()
        {
            GetComponentsInChildren(_listModels);
            
            foreach (var model in _listModels)
            {
                model.Inject(this);
            }
        }

        private void OnApplicationQuit()
        {
            SaveData();
        }

        private void OnApplicationPause(bool pauseStatus)
        {
            if(pauseStatus)
                SaveData();
        }


        public T LoadData<T>() where T: AData, new()
        {
            var key = GenerateKey<T>();
            if (_loadedData.ContainsKey(key))
            {
                return (T)_loadedData[key];
            }

            string json = PlayerPrefs.GetString(key, "");
            T result = null;
            if (string.IsNullOrEmpty(json))
            {
                result = new T();
            }
            else
            {
                result = JsonUtility.FromJson<T>(json); 
            }
            _loadedData.Add(key, result);
            _debugListData.Add(new DebugData(result));
            return result;
            
        }
        
        private void SaveData()
        {
            foreach (var data in _loadedData)
            {
                string json = JsonUtility.ToJson(data.Value);
                PlayerPrefs.SetString(data.Key, json);
            }
        }

        
        private string GenerateKey<T>()
        {
            return  $"{typeof(T).Name}_Data";
        }
        
        [ContextMenu("ClearSaved")]
        private void ClearSaved()
        {
            PlayerPrefs.DeleteAll();
        }
        
        [ContextMenu("CollectModels")]
        private void CollectModels()
        {
            _listModels.Clear();

            _listModels = GetComponentsInChildren<AModel>().ToList();
        }
    }
}