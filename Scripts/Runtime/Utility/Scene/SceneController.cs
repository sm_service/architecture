﻿using MCOV;
using UnityEngine.SceneManagement;

namespace Utility.Scene
{
    public class SceneController : AController
    {
        public void LoadScene(string nextScene)
        {
            SceneManager.LoadScene(nextScene);
        }
    }
}