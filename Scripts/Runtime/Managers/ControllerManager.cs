﻿using System.Collections.Generic;
using System.Linq;
using MCOV;
using UnityEngine;
using Utility.Scene;

namespace Managers
{
    public class ControllerManager : MonoBehaviour
    {
        public static ControllerManager Manager => _manager;
        private static ControllerManager _manager;
        
        private List<AController> _listControllers = new List<AController>();
        
        
        [SerializeField]
        private string nextScene;

        private void Awake()
        {
            _manager = this;
            GetComponentsInChildren(_listControllers);
            
            foreach (var controller in _listControllers)
            {
                controller.Inject(this);
            }
            
            foreach (var controller in _listControllers)
            {
                controller.Initialize();
            }
            
            foreach (var controller in _listControllers)
            {
                controller.Subscribe(); 
            }

            var sceneLoaderController = GetController<SceneController>();
            sceneLoaderController.LoadScene(nextScene);
            //sceneLoaderController.SceneLoaded(sceneInitializeName);
        }


        private void OnDestroy()
        {
            foreach (var controller in _listControllers)
            {
                controller.Unsubscribe();
            }
        }


        public T GetController<T>() where T : AController
        {
            foreach (var controller in _listControllers)
            {
                if (controller is T controllerResult)
                {
                    return controllerResult;
                }
            }

            Debug.LogError($"Не найден контроллер {typeof(T).FullName}");
            return null;
        }
        
        public IController GetControllerForInterface<T>() where T : IController
        {
            foreach (var controller in _listControllers)
            {
                if (controller is T controllerResult)
                {
                    return controllerResult;
                }
            }

            Debug.LogError($"Не найден контроллер {typeof(T).FullName}");
            return null;
        }
        
        public IReadOnlyList<T> GetControllersForInterface<T>() where T : IController
        {
            List<T> result = new List<T>();
            foreach (var controller in _listControllers)
            {
                if (controller is T controllerResult)
                {
                    result.Add(controllerResult);
                }
            }

            return result;
        }

    }
}

