﻿using Managers;
using UnityEngine;

namespace MCOV
{
    public class AObject<T> : MonoBehaviour where T : AController
    {
        private ControllerManager ControllerManager => ControllerManager.Manager;
        
        
        protected T Controller => _controller;
        
        private T _controller;

        private bool isStarted = false;
        
        private void Awake()
        {
            _controller = ControllerManager.GetController<T>();
            Initialize();
        }



        private void OnEnable()
        {
            Subscribe();
            
            if (isStarted)
                Show();

        }

        private void Start()
        {
            isStarted = true;
            Show();
        }

        private void OnDisable()
        {
            Hide();
                
            Unsubscribe();
            
            
        }
        
        protected virtual void Initialize()
        {
            
        }

        protected virtual void Show()
        {
        }
        protected virtual void Hide()
        {
        }

        protected virtual void Subscribe()
        {
        }
        
        protected virtual void Unsubscribe()
        {
        }

        
    }
}