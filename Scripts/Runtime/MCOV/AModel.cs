﻿using Managers;
using UnityEngine;

namespace MCOV
{
    public class AData
    {
        
    }

    public class AModel : MonoBehaviour
    {

        private ModelManager _modelManager;


        public void Inject(ModelManager modelManager)
        {
            _modelManager = modelManager;
        }


        protected T LoadData<T>() where T: AData, new()
        {
            return _modelManager.LoadData<T>();
            
        }
    }
}