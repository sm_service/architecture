﻿using Managers;
using UnityEngine;

namespace MCOV
{
    public interface IController
    {
    }

    public abstract class AController : MonoBehaviour, IController
    {

        public virtual void Inject(ControllerManager controllerManager)
        {
        }

        public virtual void Initialize()
        {
            
        }


        public virtual void Subscribe()
        {
            
        }
        
        public virtual void Unsubscribe()
        {
            
        }
        
    }
}